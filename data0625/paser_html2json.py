from bs4 import BeautifulSoup
import json

with open("TopicsTable.html", 'r') as treeFile:
      the_page = treeFile.read()

soup = BeautifulSoup(the_page)
nodes = soup("p")

jsonDict = {}
fake_node_index = 0
fake_node_array = []
for node in nodes:
      jsonDict[node["name"]] = {}
      jsonDict[node["name"]]["parent"] =  node["parent"]
      jsonDict[node["name"]]["percentage"] = node["percentage"]
      labelArray = node.text.split(" ")
      jsonDict[node["name"]]["label"] = labelArray[2:len(labelArray)-1]
      jsonDict[node["name"]]["children"] = []
      jsonDict[node["name"]]["cluster"] = ""
      jsonDict[node["name"]]["clusterid"] = ""
      jsonDict[node["name"]]["model_level"] = int(node["level"])
      if jsonDict[node["name"]]["model_level"] == 6:
            fake_node_name = 'fake_node_' + str(fake_node_index)
            jsonDict[node["name"]]["parent"] = fake_node_name
            fake_node_array.append(fake_node_name)
            fake_node_index = fake_node_index + 1
            
      
for fake_node_name in fake_node_array:
      jsonDict[fake_node_name] = {}
      jsonDict[fake_node_name]["parent"] =  'fake_root'
      jsonDict[fake_node_name]["percentage"] = ''
      jsonDict[fake_node_name]["label"] = []
      jsonDict[fake_node_name]["children"] = []
      jsonDict[fake_node_name]["cluster"] = ""
      jsonDict[fake_node_name]["clusterid"] = ""
      jsonDict[fake_node_name]["model_level"] = 7

jsonDict['fake_root'] = {}
jsonDict['fake_root']["parent"] =  'none'
jsonDict['fake_root']["percentage"] = ''
jsonDict['fake_root']["label"] = []
jsonDict['fake_root']["children"] = []
jsonDict['fake_root']["cluster"] = ""
jsonDict['fake_root']["clusterid"] = ""
jsonDict['fake_root']["model_level"] = 8


with open('jsonDict_parent.json', 'w') as jsonFile:
      json.dump(jsonDict, jsonFile)

      
for nodeName in jsonDict:
      unknownLevel = True
      current = jsonDict[nodeName]
      while (unknownLevel):
            if current["parent"] == 'none':
                   unknownLevel = False
            else:
                  jsonDict[current['parent']]["children"].append(nodeName)
                  current = jsonDict[current['parent']]

jsonDict['fake_root']["children"] = []

with open('jsonDict_children.json', 'w') as jsonFile:
      json.dump(jsonDict, jsonFile)
