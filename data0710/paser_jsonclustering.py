import json
import graphviz as gv

with open("jsonDict_children.json", 'r') as jsonFile:
      jsonDict = json.load(jsonFile)
      
def clusterPaser(jsonDict, clusterList):
      for clusterName in clusterList:
            children = jsonDict[clusterName]["children"] 
            for child in children:
                  jsonDict[child]["cluster"] = clusterName
                  jsonDict[child]["clusterid"] = clusterList.index(clusterName)
      # make fake_node have distinct clusters
      for itemName in jsonDict:
            if jsonDict[itemName]["cluster"] == '':
                  if itemName in clusterList:
                        jsonDict[itemName]["cluster"] =  itemName
                        jsonDict[itemName]["clusterid"] = clusterList.index(itemName)
                  else:
                        jsonDict[itemName]["clusterid"] =  len(clusterList) + 1

      # make fake_node have same cluster to fake_root
      '''
      for itemName in jsonDict:
            if jsonDict[itemName]["cluster"] == '':
                  jsonDict[itemName]["clusterid"] =  len(clusterList) + 1
      '''
      
      return jsonDict

clusterRef = []                           
for itemName in jsonDict:
      if jsonDict[itemName]["model_level"] == 6:
            clusterRef.append(itemName)

jsonCluster = clusterPaser(jsonDict, clusterRef)
with open("jsonDict_cluster.json", 'w') as jsonFile:
      json.dump(jsonCluster, jsonFile)



def jsonPaser(json, jsonCluster):
      graphDot = gv.Graph(format='svg')
      for itemName in json:
            clusterId = jsonCluster[itemName]["clusterid"]
            graphDot.node(itemName, cluster = str(clusterId))
            if json[itemName]["parent"] == 'none':
                  continue
            if json[itemName]["model_level"] == 6:
                  mi_parent = json[itemName]["mi"].split(",")[0]
                  if mi_parent != "":
                        graphDot.edge(itemName, mi_parent)
            graphDot.edge(itemName, json[itemName]["parent"])
      return graphDot

dotCluster = jsonPaser(jsonDict, jsonCluster)
with open("dot_before_layout.gv", 'w') as dotFile:
      dotFile.write(dotCluster.source)


