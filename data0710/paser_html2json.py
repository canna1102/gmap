from bs4 import BeautifulSoup
import json

with open("TopicsTable.html", 'r') as treeFile:
      the_page = treeFile.read()

soup = BeautifulSoup(the_page)
nodes = soup("p")

jsonDict = {}
fake_node_index = 0
fake_node_array = []
for node in nodes:
      jsonDict[node["name"]] = {}
      jsonDict[node["name"]]["parent"] =  node["parent"]
      jsonDict[node["name"]]["percentage"] = node["percentage"]
      try:
            jsonDict[node["name"]]["mi"] =  node["parent"] + ',' + node["mi"]
      except:
            jsonDict[node["name"]]["mi"] = ""
      labelArray = node.text.split(" ")
      jsonDict[node["name"]]["label"] = labelArray[2:len(labelArray)-1]
      jsonDict[node["name"]]["children"] = []
      jsonDict[node["name"]]["cluster"] = ""
      jsonDict[node["name"]]["clusterid"] = ""
      jsonDict[node["name"]]["model_level"] = int(node["level"])
      if jsonDict[node["name"]]["model_level"] == 6:
            jsonDict[node["name"]]["parent"] = 'fake_root'

jsonDict['fake_root'] = {}
jsonDict['fake_root']["parent"] =  'none'
jsonDict[node["name"]]["mi"] =  ''
jsonDict['fake_root']["percentage"] = ''
jsonDict['fake_root']["label"] = []
jsonDict['fake_root']["children"] = []
jsonDict['fake_root']["cluster"] = ""
jsonDict['fake_root']["clusterid"] = ""
jsonDict['fake_root']["model_level"] = 7


with open('jsonDict_parent.json', 'w') as jsonFile:
      json.dump(jsonDict, jsonFile)

      
for nodeName in jsonDict:
      unknownLevel = True
      current = jsonDict[nodeName]
      while (unknownLevel):
            if current["parent"] == 'none':
                   unknownLevel = False
            else:
                  jsonDict[current['parent']]["children"].append(nodeName)
                  current = jsonDict[current['parent']]

jsonDict['fake_root']["children"] = []

with open('jsonDict_children.json', 'w') as jsonFile:
      json.dump(jsonDict, jsonFile)
