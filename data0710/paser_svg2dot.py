from bs4 import BeautifulSoup
import json
import graphviz as gv
import csv

with open("jsonDict_cluster.json", 'r') as refFile:
      jsonRef = json.load(refFile)
      
with open("map_layout.svg", 'r') as treeFile:
      the_page = treeFile.read()
      
soup = BeautifulSoup(the_page)
gs = soup("g")

graphDot = gv.Graph(format='svg')
for g in gs:
      if g["class"][0] == "edge":
            nodes = g("title")[0].text.split("--")
            if 'fake' in nodes[0] or 'fake' in nodes[1]:
                  continue
            if  abs(jsonRef[nodes[0]]["model_level"] - jsonRef[nodes[1]]["model_level"]) > 1:
                  continue
            graphDot.edge(nodes[0], nodes[1])
      if g["class"][0] == "node":
            node = g("title")[0].text
            if 'fake' in node:
                  continue
            pos_x = g("text")[0]["x"]
            pos_y = g("text")[0]["y"]
            pos_y_dot = str(3.7 - float(g("text")[0]["y"]))
            clusterid = jsonRef[node]["clusterid"]
            graphDot.node(node,  cluster = str(clusterid), pos =  pos_x+','+pos_y_dot)
            jsonRef[node]["svg_pos"]= [pos_x, pos_y]

allRef = []
for node in jsonRef:
      pos_children_x = []
      pos_children_y = []
      clusterid = jsonRef[node]["clusterid"]
      if 'fake' in node:
            continue
      if jsonRef[node]['model_level'] == 1:
            jsonRef[node]["compute_pos"] = jsonRef[node]["svg_pos"]
            svg_pos = jsonRef[node]["svg_pos"]
            # test
            # graphDot.node(node,  cluster = str(clusterid), pos =  svg_pos[0]+','+str(3.7 - float(svg_pos[1])))
      else:
            for child in jsonRef[node]["children"]:
                  pos_children_x.append(float(jsonRef[child]["svg_pos"][0]))
                  pos_children_y.append(float(jsonRef[child]["svg_pos"][1]))
            avg_x = sum(pos_children_x)/len(pos_children_x)
            avg_y = sum(pos_children_y)/len(pos_children_y) 
            jsonRef[node]["compute_pos"] = [str(avg_x), str(avg_y)]
            # test            
            # graphDot.node(node,  cluster = str(clusterid), pos =  str(avg_x)+','+str(3.7 - avg_y))
      jsonRef[node]["name"] = node
      allRef.append(jsonRef[node])

with open("allRef.json", 'w') as refFile:
      json.dump(allRef, refFile)

with open("dot_after_layout.dot", 'w') as treeFile:
      treeFile.write(graphDot.source)


with open("allRef.csv", "wb") as archive_file:
      f = csv.writer(archive_file)
      f.writerow(["name", "parent", "clusterid", "label", "compute_pos", "svg_pos", "model_level", "percentage", "mi", "children"])
      for row in allRef:
            f.writerow([row["name"], row["parent"], row["clusterid"],\
                    ','.join(row["label"]),','.join(row["compute_pos"]),\
                        ','.join(row["svg_pos"]),row["model_level"],\
                        row["percentage"],row["mi"],','.join(row["children"])])
