from bs4 import BeautifulSoup
import json

with open("allRef.json", 'r') as refFile:
      allRef = json.load(refFile)

with open("map_refine_layout.svg", 'r') as treeFile:
      the_page = treeFile.read()

resultDict = {}
resultDict["node"] = allRef
resultDict["polygon"] = []
resultDict["svg"] = {}
resultDict["g"] = {}

soup = BeautifulSoup(the_page)
polygons = soup("polygon")
svg = soup("svg")[0]
g = soup("g")[0]

resultDict["svg"]["width"] = svg["width"]
resultDict["svg"]["height"] = svg["height"]
resultDict["svg"]["viewBox"] = svg["viewbox"]
resultDict["g"]["id"] = g["id"]
resultDict["g"]["class"] = g["class"][0]
resultDict["g"]["transform"] = g["transform"]

for polygon in polygons:
      temp = {}
      if polygon["stroke"] == "transparent":
            continue
      temp["fill"] = polygon["fill"]
      temp["stroke"] = polygon["stroke"]
      temp["points"] = polygon["points"]

      resultDict["polygon"].append(temp)


with open("all.json", 'w') as jsonFile:
      json.dump(resultDict, jsonFile)
