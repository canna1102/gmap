This is the project for visualizing a large topic tree base on Gmap and d3.js techniques.

The demo is available at http://ihome.ust.hk/~yyangao/launch-Gmap/index.html.

Simple instruction on usage:

* zoom in/out to expand/shrink tree
* drag to pan the canvas
* move pointer over the node labels to see the full topic contents

As a beginner of Javascript, I wrote this project without any platform so that the structure may not be as clear as the future ones. If you have any problem regarding to my snippets, please feel free to contact me. 

Cheers!